﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    public class List<T>
    {
        public Node<T> Head { get; set; }
        public Node<T> Tail { get; set; }
        public int Size { get; set; } = 0;
        public void Add(T info)
        {
            Node<T> node = new Node<T> { Info = info, Next = null };
            if (Head == null)
            {
                Head = node;
                Tail = Head;
            }
            else
            {
                node.Past = Tail;
                Tail.Next = node;
            }
            Tail = node;
            Size++;
        }
        public Node<T> Get(int index)
        {
            Node<T> node = new Node<T>();
            node = Head;

            for (int i = 0; i < index; i++)
                node = node.Next;

            return node;
        }
        public void Clear()
        {
            Head = null;
            Tail = null;
            Size = 0;
        }
    }
    public class Node<T>
    {
        public T Info { get; set; }
        public Node<T> Next { get; set; }
        public Node<T> Past { get; set; }
    }
}
