﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    class Sort
    {
        public List<int> Sorting(List<int> list)
        {
            Node<int> curr = new Node<int>();
            List<int> res = new List<int>();

            curr = list.Head;
            while (curr != null)
            {
                Node<int> node = new Node<int> { Info = curr.Info, Next = null };
                if (res.Size == 0)
                {
                    res.Add(curr.Info);
                }
                else
                {
                    Node<int> tmp = new Node<int>();
                    tmp = res.Head;
                    while (tmp.Next != null && tmp.Info < curr.Info)
                    {
                        tmp = tmp.Next;
                    }

                    Node<int> buf = new Node<int> { Info = curr.Info, Next = null, Past = null };

                    if (tmp == res.Head)
                    {
                        buf.Next = tmp;
                        tmp.Past = buf;

                        res.Head = buf;
                    }
                    else
                    {
                        if (tmp == res.Tail)
                        {
                            tmp.Next = buf;
                            buf.Past = tmp;
                        }
                        else
                        {
                            tmp = tmp.Past;
                            buf.Next = tmp.Next;
                            tmp.Next = buf;
                            buf.Past = tmp;
                            tmp.Past = buf;
                        }
                    }
                }
                curr = curr.Next;
            }
            return res;
        }
    }
}
