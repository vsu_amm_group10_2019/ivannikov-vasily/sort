﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Delay(int timeDelay)
        {
            int i = 0;
            var delayTimer = new System.Timers.Timer();
            delayTimer.Interval = timeDelay;
            delayTimer.AutoReset = false;
            delayTimer.Elapsed += (s, args) => i = 1;
            delayTimer.Start();
            while (i == 0) { };
        }
        private void MakeGrid(int n, List<int> list, DataGridView dgv)
        {
            dgv.RowCount = 1;
            dgv.ColumnCount = list.Size;
            for (int i = 0; i < list.Size; i++)
            {
                dgv.Rows[0].Cells[i].Value = list.Get(i).Info;
            }
            dgv.Refresh();
            Delay(1000);
        }
        private void Sort(List<int> list)
        {
            List<int> res = new List<int>();
            Node<int> curr = new Node<int>();
            curr = list.Head;

            dataGridView2.RowCount = 1;
            dataGridView2.ColumnCount = 0;
            
            while (curr != null)
            {
                dataGridView1.Rows[0].Cells[dataGridView2.ColumnCount].Style.BackColor = System.Drawing.Color.Gray;
                dataGridView1.Refresh();
                dataGridView2.ColumnCount += 1;

                Node<int> node = new Node<int> { Info = curr.Info, Next = null };

                res.Add(curr.Info);

                if (res.Head == null)
                    dataGridView2.Rows[0].Cells[0].Value = curr.Info;
                else
                {
                    int col = dataGridView2.ColumnCount - 1;
                    while (col > 0 && Convert.ToInt32(dataGridView2.Rows[0].Cells[col - 1].Value) > curr.Info)
                    {
                        dataGridView2.Rows[0].Cells[col].Value = dataGridView2.Rows[0].Cells[col - 1].Value;
                        dataGridView2.Rows[0].Cells[col - 1].Value = curr.Info;
                        col--;
                    }
                    dataGridView2.Rows[0].Cells[col].Value = curr.Info;
                }

                dataGridView2.Refresh();
                Delay(1000);
                curr = curr.Next;
                dataGridView1.Rows[0].Cells[dataGridView2.ColumnCount - 1].Style.BackColor = System.Drawing.Color.White;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int size = 10;
            List<int> list = new List<int>();
            for (int i = 0; i < size; i++)
            {
                list.Add(rnd.Next(1, 20));
            }
            MakeGrid(list.Size, list, dataGridView1);
            Sort(list);
            MessageBox.Show("Отсортировано");
        }
    }
}
